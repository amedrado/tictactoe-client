# Tic Tac Toe client 

I wrote this simple client to test the Tic Tac Toe Backend.

I'm not a big fan of Javascript, so I wrote this client using only Raw PHP and HTML.

The usage is very simple:

* Clone this repository
* Start the PHP Built-in Webserver: `php -S 0.0.0.0:8181 index.php`
* Point your browser to: http://localhost:8181 

I was focused on backend, so I made this client in the most simplest way, using a html simple form, so you need to type the _X_ into the field and press the button. After that, the bot will do its move.
The form submits to the same page, who get the parameters and make the requests using cURL. Pretty simple.


