<?php
require 'curl_functions.php';
$result = new stdclass;
$result->boardState = [['','',''],['','',''],['','','']];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = prepare_board_state();
    $result = send_request_move($fields);
    echo $result;
    $result = json_decode($result);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adler's Tic Tac Toe</title>
</head>
<body>
<h1>
    Tic Tac Toe
</h1>
<br>
<form method="post">
    <table class="table table-bordered">
        <tbody align="center">
        <tr>
            <td><input type="text" name="x_0_y_0" value="<?php echo ($result->boardState[0][0]) ? $result->boardState[0][0] : '' ?>" <?php echo ($result->boardState[0][0]) ? 'readonly="readonly"' : ''?></td>
            <td><input type="text" name="x_0_y_1" value="<?php echo ($result->boardState[0][1]) ? $result->boardState[0][1] : '' ?>" <?php echo ($result->boardState[0][1]) ? 'readonly="readonly"' : ''?>></td>
            <td><input type="text" name="x_0_y_2" value="<?php echo ($result->boardState[0][2]) ? $result->boardState[0][2] : '' ?>" <?php echo ($result->boardState[0][2]) ? 'readonly="readonly"' : ''?>></td>
        </tr>
        <tr>
            <td><input type="text" name="x_1_y_0" value="<?php echo ($result->boardState[1][0]) ? $result->boardState[1][0] : '' ?>" <?php echo ($result->boardState[1][0]) ? 'readonly="readonly"' : ''?>></td>
            <td><input type="text" name="x_1_y_1" value="<?php echo ($result->boardState[1][1]) ? $result->boardState[1][1] : '' ?>" <?php echo ($result->boardState[1][1]) ? 'readonly="readonly"' : ''?>></td>
            <td><input type="text" name="x_1_y_2" value="<?php echo ($result->boardState[1][2]) ? $result->boardState[1][2] : '' ?>" <?php echo ($result->boardState[1][2]) ? 'readonly="readonly"' : ''?>></td>
        </tr>
        <tr>
            <td><input type="text" name="x_2_y_0" value="<?php echo ($result->boardState[2][0]) ? $result->boardState[2][0] : '' ?>" <?php echo ($result->boardState[2][0]) ? 'readonly="readonly"' : ''?>></td>
            <td><input type="text" name="x_2_y_1" value="<?php echo ($result->boardState[2][1]) ? $result->boardState[2][1] : '' ?>" <?php echo ($result->boardState[2][1]) ? 'readonly="readonly"' : ''?>></td>
            <td><input type="text" name="x_2_y_2" value="<?php echo ($result->boardState[2][2]) ? $result->boardState[2][2] : '' ?>" <?php echo ($result->boardState[2][2]) ? 'readonly="readonly"' : ''?>></td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="button" value="Restart" onclick="window.location.href='/'"><input type="submit" value="Play">
            </td>
        </tr>
        </tbody>
    </table>
    <input type="hidden" name="current_board_state" value="">
</form>
</body>
</html>
